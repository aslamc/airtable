module.exports = {
    entry: './src/app.js',
    output: {
        path: __dirname + '/build/',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            }
        ]
    },
    devtool: 'source-map',
    node: {
        fs: "empty"
    },
    devtool: "source-map"
};