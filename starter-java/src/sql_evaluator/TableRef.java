package sql_evaluator;

import com.fasterxml.jackson.annotation.*;

/**
 * These appear in the FROM clause.
 */
public final class TableRef extends Node {
    public final Source source;
    public final String as;

    @JsonCreator
    public TableRef(@JsonProperty("source") Source source, @JsonProperty("as") String as) {
        if (source == null) throw new IllegalArgumentException("'source' can't be null");
        if (as == null) throw new IllegalArgumentException("'as' can't be null");
        this.source = source;
        this.as = as;
    }

    @JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
    @JsonSubTypes({
        @JsonSubTypes.Type(value=Source.File.class, name="file"),
        // There's currently only one source subtype, but there may be more in the future (e.g. sub-queries)
    })
    public static abstract class Source extends Node {
        public static final class File extends Source {
            @JsonValue
            public final String name;

            @JsonCreator(mode=JsonCreator.Mode.DELEGATING)
            public File(String name) {
                if (name == null) throw new IllegalArgumentException("'name' can't be null");
                this.name = name;
            }
        }
    }
}
