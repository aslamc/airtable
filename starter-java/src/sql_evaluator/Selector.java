package sql_evaluator;

import com.fasterxml.jackson.annotation.*;

/**
 * These appear in the SELECT clause.
 */
final class Selector extends Node {
    public final Source source;
    public final String as;

    @JsonCreator
    public Selector(@JsonProperty("source") Source source, @JsonProperty("as") String as) {
        if (source == null) throw new IllegalArgumentException("'source' can't be null");
        if (as == null) throw new IllegalArgumentException("'as' can't be null");
        this.source = source;
        this.as = as;
    }

    @JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
    @JsonSubTypes({
        @JsonSubTypes.Type(value=Source.Column.class, name="column"),
        // There's currently only one source subtype, but there may be more in the future (e.g. aggregates)
    })
    public static abstract class Source extends Node {
        public static final class Column extends Source {
            @JsonValue
            public final ColumnRef ref;

            @JsonCreator(mode=JsonCreator.Mode.DELEGATING)
            public Column(ColumnRef ref) {
                if (ref == null) throw new IllegalArgumentException("'ref' can't be null");
                this.ref = ref;
            }
        }
    }
}

