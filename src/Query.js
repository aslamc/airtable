export class QueryTable {
    constructor(tableName: String, proxyName: String) {
        this.name = tableName;
        this.proxy = proxyName;
    }
}

export class QueryField {
    constructor(columnName: String, columnProxy: String, tableName: String, tableProxyName: String) {
        this.tableName = tableName;
        this.tableProxy = tableProxyName;
        this.columnName = columnName;
        this.columnProxy = columnProxy;
    }
}

export class Query {
    constructor(query: JSON) {
        this.query = query;
    }

    getQueryTables(): Array<QueryTable> {
        let tables = this.query["from"];
        let tableNames = [];
        tables.forEach(table => {
            const tableName = table["source"]["file"];
            const tableProxy = table["as"];
            tableNames.push(new QueryTable(tableName, tableProxy));
        });
        return tableNames;
    }

    getFields(): Array<QueryField> {
        let tableProxyMap = this.getTableProxyMap();
        let fields = [];
        this.query["select"].forEach(select => {
            const tableProxy = select["source"]["column"]["table"];
            const columnName = select["source"]["column"]["name"];
            const columnProxy = select["as"];
            fields.push(new QueryField(columnName, columnProxy, tableProxyMap.get(tableProxy), tableProxy));
        });
        return fields;
    }

    getFieldsGropuedByTableProxy(): Map<String, QueryField> {
        let result = new Map();
        this.getFields().forEach((f: QueryField) => {
            if (!result.has(f.tableProxy))
                result.set(f.tableProxy, []);
            result.get(f.tableProxy).push(f)
        });
        return result;
    }

    /*
        @returns: a dictionary with the key as the proxy, and the QueryTable Object ask the key
     */
    getTableProxyMap(): Map<String, String> {
        let tableProxyMap = new Map();
        this.getQueryTables().forEach((t: QueryTable) => {
            tableProxyMap.set(t.proxy, t.name);
        });
        return tableProxyMap;
    }
}