// import * as TablePrinter from 'cli-table'

const TableP = require('cli-table2');


export class Database {
    constructor() {
        this.db = new Map();
    }

    insert(tableName: String, colums: Array<Column>, data: JSON) {
        this.db.set(tableName, new Table(tableName, colums, data));
    }

    hasTable(key) {
        return this.db.has(key);
    }

    getTable(name: String): Table {
        return this.db.get(name);
    }
}

export class Table {
    constructor(name: String, columns: Array<Column>, data: Object) {
        this.name = name;
        this.columns = columns;
        this.data = data;
    }

    getColumnData(column: String) {
        const colIdx = this.columns.findIndex(c => c.name === column);
        return this.data.map(r => r[colIdx]);
    }

    prettyPrint() {
        const cols = this.columns.map((c: Column) => c.name);
        const table = new TableP({
            head: cols,
            // colWidths: cols.map(_ => 5),
            chars: {'mid': '', 'left-mid': '', 'mid-mid': '', 'right-mid': ''}
        });
        this.data.forEach(d => table.push(d));
        console.log(table.toString());
    }
}

export class Column {
    constructor(name: String, type: String) {
        this.name = name;
        this.type = type;
    }
}

