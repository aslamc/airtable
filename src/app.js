import {QueryExecutor} from "./QueryExecutor";

const commandLineArgs = require('command-line-args');

const optionDefinitions = [{name: 'src', type: String, multiple: false, defaultOption: true},];
const options = commandLineArgs(optionDefinitions);
const results = new QueryExecutor(options['src']);
