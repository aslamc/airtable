// @flow
'use strict';

import {_, zip} from 'underscore'
import {Column, Database, Table} from "./Database";
import {Query, QueryField, QueryTable} from "./Query";
import * as fs from 'fs';

// const fs = require('fs');

export class QueryExecutor {
    query: Query;
    db: Database;
    joinedTables: Table;

    constructor(filePath: string) {
        this.query = new Query(this.loadJSONFromFile(filePath));
        this.loadTableDataFromFile();
        this.joinTables();
        this.processQuery();
    }

    loadJSONFromFile(filePath: string): any {
        let data: JSON;
        try {
            data = JSON.parse(fs.readFileSync("examples/" + filePath, "utf-8"));
        } catch (e) {
            console.error("File doesn't exist");
        }
        return data;
    }

    loadTableDataFromFile(): void {
        // Load Data from Files
        this.db = new Database();
        const tables = this.query.getQueryTables();
        tables.forEach(qt => {
            // Avoid duplicate storage of data
            if (this.db.hasTable(qt.name))
                return;

            const dataFileName = `${qt.name}.table.json`;
            const tableData = this.loadJSONFromFile(dataFileName);
            const columnInfo = tableData.shift().map(c => new Column(c[0], c[1]));
            this.db.insert(qt.name, columnInfo, tableData);
        });
    }

    processQuery(): void {
        // let results = new Map();

        const columns = this.query.getFields().map(field =>  {
           let col = this.db.getTable(field.tableName).columns.find((c: Column) => c.name === field.columnName);
           return new Column(field.columnProxy, col.type);
        });
        const resultData = this.executeTableSearch(this.joinedTables, this.query.getFields());
        const table = new Table("result", columns, resultData);
        table.prettyPrint();
    }

    executeTableSearch(table: QueryTable, fields: Array<QueryField>) {
        const colIdxs = fields.map((f: QueryField) => {
            return this.joinedTables.columns.findIndex((c: Column) => c.name === `${f.tableProxy}.${f.columnName}`);
        });
        return this.joinedTables.data.map(tableRow => {
            let filteredTableRow = [];
            colIdxs.forEach(idx => {
                filteredTableRow.push(tableRow[idx]);
            });
            return filteredTableRow;
        });
        // return zip.apply(_, filteredColumns);
    }

    joinTables() {
        const qTables = this.query.getQueryTables();
        const firstQTable = qTables.shift();
        const firstDBTable = this.db.getTable(firstQTable.name);
        let columns = firstDBTable.columns.map((c: Column) => new Column(`${firstQTable.proxy}.${c.name}`, c.type));
        const joinedTable = [];
        qTables.forEach((t: QueryTable) => {
            const dbTable = this.db.getTable(t.name);
            const tData = dbTable.data;
            columns = columns.concat(dbTable.columns.map((c: Column) => new Column(`${t.proxy}.${c.name}`, c.type)));
            tData.forEach(d => {
                firstDBTable.data.forEach(firstTD => {
                    joinedTable.push(d.concat(firstTD));
                });
            });
        });

        this.joinedTables = new Table("joinedTable", columns, joinedTable);
        // this.joinedTables.prettyPrint();
    }
}
